       IDENTIFICATION DIVISION. 
       PROGRAM-ID. DATA1.
       AUTHOR. BENJAMAS.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 NUM1 PIC 999 VALUE ZEROS .
       01 NUM2 PIC 999 VALUE 15.
       01 TEX-RATE PIC V99 VALUE .35.
       01 CUSTOMER-NAME PIC x(15) VALUE "Benjamas".

       PROCEDURE DIVISION .
       Begin.
           DISPLAY "NUM 1 " NUM1 .
           DISPLAY "NUM 2 " NUM2 .
           DISPLAY "TEX-RATE " TEX-RATE .
           DISPLAY "CUSTOMER-NAME " CUSTOMER-NAME .

