       IDENTIFICATION DIVISION. 
       PROGRAM-ID. DATA5.
       AUTHOR. Benjamas.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 GRADE-DATA PIC X(90) VALUE "39030261BENJAMAS        886345593B
      -    " 886352593D+886342193B+886478593C 886481592C+886491591A ".
       01 GRADE REDEFINES GRADE-DATA .
           03 STU-ID PIC 9(8).
           03 STU-NAME PIC X(16).
           03 SUB1.
              05 SUB-CODE1 PIC 9(8).
              05 SUB-UNIT1 PIC 9.
              05 SUB_GRADE1 PIC X(2).

           03 SUB2.
              05 SUB-CODE2 PIC 9(8).
              05 SUB-UNIT2 PIC 9.
              05 SUB_GRADE2 PIC X(2).

           03 SUB3.
              05 SUB-CODE3 PIC 9(8).
              05 SUB-UNIT3 PIC 9.
              05 SUB_GRADE3 PIC X(2).

           03 SUB4.
              05 SUB-CODE4 PIC 9(8).
              05 SUB-UNIT4 PIC 9.
              05 SUB_GRADE4 PIC X(2).

           03 SUB5.
              05 SUB-CODE5 PIC 9(8).
              05 SUB-UNIT5 PIC 9.
              05 SUB_GRADE5 PIC X(2).

           03 SUB6.
              05 SUB-CODE6 PIC 9(8).
              05 SUB-UNIT6 PIC 9.
              05 SUB_GRADE6 PIC X(2).

       66 STUDENT-ID RENAMES STU-ID.
       66 STUDEN-INFO RENAMES STU-ID THRU STU-NAME .
       01 STUCODE REDEFINES GRADE-DATA  .
           05 STU-YEAR PIC 9(2).
           05 FILLER PIC X(6).
           05 STU-SHORT-NAME PIC X(3).

       PROCEDURE DIVISION .
       Begin.
           MOVE GRADE-DATA TO GRADE .
           DISPLAY GRADE .

           DISPLAY " ".

           DISPLAY "Subject 1 ".
           DISPLAY "Code : " SUB-CODE1 .
           DISPLAY "Unit : " SUB-UNIT1 .
           DISPLAY "Grade : " SUB_GRADE1 .

           DISPLAY " ".

           DISPLAY "Subject 2 ".
           DISPLAY "Code : " SUB-CODE2 .
           DISPLAY "Unit : " SUB-UNIT2 .
           DISPLAY "Grade : " SUB_GRADE2 .

           DISPLAY " ".

           DISPLAY "Subject 3 ".
           DISPLAY "Code : " SUB-CODE3 .
           DISPLAY "Unit : " SUB-UNIT3  .
           DISPLAY "Grade : " SUB_GRADE3  .

           DISPLAY " ".

           DISPLAY "Subject 4 ".
           DISPLAY "Code : " SUB-CODE4  .
           DISPLAY "Unit : " SUB-UNIT4 .
           DISPLAY "Grade : " SUB_GRADE4 .

           DISPLAY " ".

           DISPLAY "Subject 5 ".
           DISPLAY "Code : " SUB-CODE5 .
           DISPLAY "Unit : " SUB-UNIT5 .
           DISPLAY "Grade : " SUB_GRADE5 .

           DISPLAY " ".

           DISPLAY "Subject 6 ".
           DISPLAY "Code : " SUB-CODE6 .
           DISPLAY "Unit : " SUB-UNIT6 .
           DISPLAY "Grade : " SUB_GRADE6 .

           DISPLAY " ".

           DISPLAY STUDENT-ID .
           DISPLAY STUDEN-INFO .
           DISPLAY STU-YEAR  STU-SHORT-NAME .

